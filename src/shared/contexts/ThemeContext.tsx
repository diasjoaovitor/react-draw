import { createContext, ReactNode, useContext } from "react"
import { ThemeProvider as MUIThemeProvider } from '@emotion/react'
import { Box, CssBaseline, Theme } from '@mui/material'
import { darkTheme } from '../themes'

type TThemContext = {
  theme: Theme
}

const ThemeContext = createContext({} as TThemContext)

export const useThemeContext = () => useContext(ThemeContext)

export const ThemeProvider: React.FC<{children: ReactNode}> = ({ children }) => {
  const theme = darkTheme
  return (
    <ThemeContext.Provider value={{ theme }}>
      <MUIThemeProvider theme={theme}>
        <CssBaseline />
        <Box
          width="100vw" 
          height="100vh" 
          display='flex' 
          flexDirection='column' 
          bgcolor={theme.palette.background.default}
        >
          {children}
        </Box>
      </MUIThemeProvider>
    </ThemeContext.Provider>
  )
}
