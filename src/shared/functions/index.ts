import { board, paper, rect } from "../states"
import { TRect, TSVGElement, TSVGGroup, TText, TTextElements } from "../types"

export const getWidth = (height: number): number => {
  const width = height * paper.width / paper.height
  return width
}

export const getTextElements = (board: TSVGGroup): TTextElements => {
  const elements = board.elements.map(element => {
    return { [ element.name ]: element }
  })
  let textElements = {}
  elements.forEach(element => textElements = { ...textElements, ...element })
  return textElements as TTextElements
}

export const getScale = (width: number, height: number): [ number, number ] => {
  const { width: w, height: h } = rect.attributes
  const scale = [ width / w, height / h ]
  return scale as [ number, number ]
}

export const getBoards = (boards: TSVGGroup[], b: TSVGGroup, boardIndex: number) => {
  const bs = boards.map((board, index) => (
    boardIndex !== index ? board : b
  ))
  return bs
}

export const getNewBoard = (boards: TSVGGroup[], numberOfElements: number): TSVGGroup | undefined => {
  const lastBoard = boards.slice(-1)[0]
  if (!lastBoard) return
  const rect = lastBoard.elements.filter(({ type }) => type === 'rect').slice(-1)[0] as TRect
  const [ _, scaleY ] = lastBoard.transform.scale
  const elements = [ ...lastBoard.elements ].slice(-numberOfElements)
  const newBoard = { 
    ...board, 
    elements: elements.map((element, index) => {
      const el = { 
        ...element,
        attributes: { 
          ...element.attributes, 
          y: element.attributes.y + rect.attributes.height * scaleY
        }
      }
      return element.type === 'text' 
        ? { ...el, textContent: (board.elements[index] as TText).textContent}
        : el
    })
  } as TSVGGroup
  return newBoard
}

export const getText = (board: TSVGGroup, name: string, value: string) => {
  const b = { ...board, elements: board.elements.map(element => {
    if (name === element.name) {
      return { 
        ...element, 
        textContent: element.name !== 'netWeight' ? 
          value.toUpperCase() : value
      }
    }
    return element
  })}
  return b
}

export const getPrice = (board: TSVGGroup, value: number) => {
  const [ unit, cents ] = value.toFixed(2).split('.')
  const b = { ...board, elements: board.elements.map(element => {
    if (element.name === 'unit') {
      return { ...element, textContent: unit }
    }
    if (element.name === 'cents') {
      return { ...element, textContent: cents }
    }
    return element
  })}
  return b
}

export const handleQuantity = (board: TSVGGroup, quantity: number, numberOfElements: number) => {
  let elements = [ ...board.elements ].slice(0, numberOfElements)
  const main = board.elements.slice(-numberOfElements)
  const rect = main.find(({ type }) => type === 'rect') as TRect
  for (let i = 0; i < quantity - 1; i++) {
    const lastBoard = elements.slice(-numberOfElements)
    const newElement = lastBoard.map(element => {
      const y = element.attributes.y + rect.attributes.height
      return { 
        ...element,
        attributes: { ...element.attributes, y }
      }
    }) as TSVGElement[]
    elements = [ ...elements, ...newElement ]
  }
  return { ...board, quantity, elements }
}