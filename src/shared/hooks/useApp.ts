import { ChangeEvent, FocusEvent, MouseEvent, useEffect, useState } from "react"
import { SelectChangeEvent } from "@mui/material"
import { rect as r, board as b } from '../states'
import { getBoards, getNewBoard, getScale, getPrice, getText, handleQuantity } from "../functions"
import { TSVGGroup } from "../types"
import { create } from "../services"

let boardIndex = 0
const numberOfElements = b.elements.length

export const useApp = () => {
  const [ rect, setRect ] = useState(r)
  const [ board, setBoard ] = useState(b)
  const [ boards, setBoards ] = useState([b])

  useEffect(() => {
    const { width, height } = rect.attributes
    const scale = getScale(width, height)
    const b = { ...board, transform: { ...board.transform, scale }}
    setBoard(b)
    setBoards(getBoards(boards, b, boardIndex))
  }, [rect])

  const handleSelectChange = (e: SelectChangeEvent) => {
    setBoard({ ...board, template: e.target.value })
  }

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target
    let b: TSVGGroup
    if (e.target.name === 'quantity' ) {
      b = handleQuantity(board, Number(value), numberOfElements)
    } else {
      b = getText(board, name, value)
    }
    setBoard(b)
    setBoards(getBoards(boards, b, boardIndex))
  }

  const handleTransform = (e: ChangeEvent<HTMLInputElement>) => {
    setRect({ 
      ...rect, 
      attributes: { ...rect.attributes, [ e.target.name ]: Number(e.target.value) }
    })
  }

  const handleBlur = (e: FocusEvent<HTMLInputElement>) => {
    const { value } = e.target
    const b = getPrice(board, Number(value))
    setBoard(b)
    setBoards(getBoards(boards, b, boardIndex))
  }

  const newBoard = () => {
    boardIndex = boards.length
    const newBoard = getNewBoard(boards, numberOfElements)
    if (!newBoard) {
      setBoard(b)
      setBoards([b])
      return
    }
    setBoard(newBoard)
    setBoards([ ...boards, newBoard ])
  }

  const handleClick = (e: MouseEvent<SVGAElement>) => {
    const index = Number(e.currentTarget.id)
    boardIndex = index
    setBoard(boards[index])
  }

  const removeBoard = () => {
    const bs = boards.filter((_, index) => boardIndex !== index)
    boardIndex = bs.length - 1
    setBoards(bs.length > 0 ? bs : [b])
  }

  const print = async () => {
    const paper = document.querySelector('.paper')
    if(!paper) return
    const file = await create(paper.innerHTML)
    const fileURL = URL.createObjectURL(file)
    window.open(fileURL)
  }

  return {
    rect, board, boards,
    handleSelectChange, handleChange, handleTransform, handleBlur,
    newBoard, handleClick, removeBoard, print
  }
}