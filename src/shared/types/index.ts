export type TRect = {
  type: string
  name: string
  attributes: {
    x: number
    y: number
    width: number
    height: number
    style: {
      fill: string
      stroke: string
      strokeWidth: number
    }
  }
}

export type TText = {
  type: string
  name: string
  textContent: string
  attributes: {
    x: number
    y: number
    textLength: number
    lengthAdjust: 'spacingAndGlyphs' | 'spacing' | 'none'
    style: {
      fill: string
      fontSize: string
      fontFamily: string
    }
  }
}

export type TSVGElement = TRect | TText

export type TSVGGroup = {
  template: string
  transform: {
    scale: [number, number]
  }
  quantity: number
  elements: TSVGElement[]
}

export type TTextElements = {
  product: TText
  brand: TText
  currency: TText
  unit: TText
  cents: TText
  separator: TText
  netWeight: TText
}
