export const create = async (html: string) => {
  const response = await fetch("http://localhost:8000/create", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ html })
  })
  const file = await response.blob()
  return file
}
