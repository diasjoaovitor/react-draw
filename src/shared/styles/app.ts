import { SxProps, Theme } from "@mui/material"

export const app: SxProps<Theme> = {
  flex: 1,
  display: 'flex',
  justifyContent: 'space-between',
  overflowY: 'hidden'
} 