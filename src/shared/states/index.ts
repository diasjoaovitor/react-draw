import { pallet } from "../styles"
import { TRect, TSVGGroup, TText } from "../types"

export const paper = {
  width: 21000,
  height: 29700 
}

export const templates = ['fina', 'fina-promoção', 'meia-folha', 'meia-folha-2', 'grande', 'grande-2']

export const board: TSVGGroup = {
  template: 'fina',
  transform: {
    scale: [1, 1]
  },
  quantity: 1,
  elements: [
    {
      type: 'rect',
      name: 'rect',
      attributes: {
        x: 800,
        y: 800,
        width: 12000,
        height: 3500,
        style: {
          fill: 'none',
          stroke: pallet.black,
          strokeWidth: 20,
        }
      }
    } as TRect,
    {
      type: 'text',
      name: 'product',
      textContent: 'NOME DO PRODUTO',
      attributes: {
        x: 1000,
        y: 2050,
        textLength: 6000,
        lengthAdjust: 'spacingAndGlyphs',
        style: {
          fill: pallet.blue,
          fontSize: `${350}mm`
        }
      }
    } as TText,
    {
      type: 'text',
      name: 'brand',
      textContent: 'MARCA DO PRODUTO',
      attributes: {
        x: 1000,
        y: 4080,
        textLength: 6000,
        lengthAdjust: 'spacingAndGlyphs',
        style: {
          fill: pallet.red,
          fontSize: `${616.666}mm`
        }
      }
    } as TText,
    {
      type: 'text',
      name: 'currency',
      textContent: 'R$',
      attributes: {
        x: 6400 + 800,
        y: 4080/2 + 350*3,
        textLength: 500,
        lengthAdjust: 'spacingAndGlyphs',
        style: {
          fill: pallet.green,
          fontSize: `${350}mm`
        }
      }
    } as TText,
    {
      type: 'text',
      name: 'unit',
      textContent: '32',
      attributes: {
        x: 7900,
        y: 3500 + 600,
        textLength: 2400,
        lengthAdjust: 'spacingAndGlyphs',
        style: {
          fill: pallet.black,
          fontSize: `${3500/3 - 40}mm`
        }
      }
    } as TText,
    {
      type: 'text',
      name: 'cents',
      textContent: '98',
      attributes: {
        x: 10400,
        y: 3200,
        textLength: 2200,
        lengthAdjust: 'spacingAndGlyphs',
        style: {
          fill: pallet.black,
          fontSize: `${800}mm`
        }
      }
    } as TText,
    {
      type: 'text',
      name: 'separator',
      textContent: ',',
      attributes: {
        x: 10300,
        y: 3700,
        textLength: 800,
        lengthAdjust: 'spacingAndGlyphs',
        style: {
          fill: pallet.black,
          fontSize: `${600}mm`
        }
      }
    } as TText,
    {
      type: 'text',
      name: 'netWeight',
      textContent: '500g',
      attributes: {
        x: 11200,
        y: 4000,
        textLength: 1300,
        lengthAdjust: 'spacingAndGlyphs',
        style: {
          fill: pallet.green,
          fontSize: `${250}mm`
        }
      }
    } as TText
  ]
}

export const rect = board.elements.find(({ type }) => type === 'rect') as TRect 
