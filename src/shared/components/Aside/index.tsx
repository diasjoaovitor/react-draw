import { ChangeEvent, FocusEvent } from 'react'
import { Box, Button, Divider, FormControl, InputLabel, MenuItem, Select, SelectChangeEvent, Stack, TextField } from '@mui/material'
import { TRect, TSVGGroup } from '../../types'
import { useThemeContext } from '../../contexts'
import { templates } from '../../states'
import { getTextElements } from '../../functions'
import * as S from './style'

type Props = {
  rect: TRect
  board: TSVGGroup
  handleSelectChange(e: SelectChangeEvent): void
  handleChange(e: ChangeEvent<HTMLInputElement>): void
  handleTransform(e: ChangeEvent<HTMLInputElement> | FocusEvent<HTMLInputElement>): void
  handleBlur(e: FocusEvent<HTMLInputElement>): void
  newBoard(): void
  removeBoard(): void
  print(): void
}

export const Aside: React.FC<Props> = ({ 
  rect, board, 
  handleSelectChange, handleChange, handleTransform, handleBlur,
  newBoard, removeBoard, print 
}) => {
  const { theme } = useThemeContext()
  const backgroundColor = theme.palette.background.paper

  const { 
    product, brand, unit, cents, netWeight
  } = getTextElements(board)

  const handleFocus = (e: FocusEvent<HTMLInputElement>) => e.currentTarget.select()
  return (
    <Box component="aside" sx={{ ...S.aside, backgroundColor }}>
      <Box padding={2} paddingBottom={0}>
        <FormControl fullWidth>
          <InputLabel id="template">Modelo</InputLabel>
          <Select
            name="template"
            labelId="template"
            label="Modelo"
            value={board.template}
            onChange={handleSelectChange}
            fullWidth
          >
            {templates.sort().map((template, index) => (
              <MenuItem key={index} value={template}>{template}</MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>
      <Divider />
      <Box padding={2} paddingBottom={0}>
        <FormControl fullWidth>
          <TextField 
            name="product" label="Nome do Produto"  
            value={product.textContent} onChange={handleChange} 
            onFocus={handleFocus}
          />
          <TextField 
            name="brand" label="Marca do Produto"  
            value={brand.textContent} onChange={handleChange} 
            onFocus={handleFocus}
          />
          <Stack direction="row" gap={2}>
            <TextField 
              name="price" label="Preço" type="number"  
              defaultValue={`${unit.textContent}.${cents.textContent}`} 
              onFocus={handleFocus} onBlur={handleBlur}
            />
            <TextField 
              name="netWeight" label="Peso Líquido" 
              value={netWeight.textContent} onChange={handleChange} 
              onFocus={handleFocus}
            />
          </Stack>
        </FormControl>
      </Box>
      <Divider />
      <Box padding={2} paddingBottom={0}>
        <FormControl fullWidth>
          <Stack direction="row" gap={2}>
            <TextField 
              name="x" label="X" type="number"  
              value={rect.attributes.x} onChange={handleTransform} 
              onFocus={handleFocus}
            />
            <TextField 
              name="y" label="Y" type="number"  
              value={rect.attributes.y} onChange={handleTransform} 
              onFocus={handleFocus}
            />
            <TextField 
              name="rotation" label="R" type="number"  
              defaultValue={0} onBlur={(e: FocusEvent<HTMLInputElement>) => handleTransform(e)} 
              onFocus={handleFocus}
            />
          </Stack>
          <Stack direction="row" gap={2}>
            <TextField 
              name="width" label="W" type="number"  
              defaultValue={rect.attributes.width} onBlur={(e: FocusEvent<HTMLInputElement>) => handleTransform(e)} 
              onFocus={handleFocus}
            />
            <TextField 
              name="height" label="H" type="number"  
              defaultValue={rect.attributes.height} onBlur={(e: FocusEvent<HTMLInputElement>) => handleTransform(e)} 
              onFocus={handleFocus}
            />
            <TextField 
              name="quantity" label="Q" type="number" 
              InputProps={{ inputProps: { min: 1 } }}
              value={board.quantity} onChange={handleChange} 
              onFocus={handleFocus}
            />
          </Stack>
        </FormControl>
      </Box>
      <Divider />
      <Stack direction="row" gap={2} padding={2}>
        <Button variant="contained" fullWidth onClick={newBoard}>
          Nova Placa
        </Button>
        <Button variant="contained" fullWidth color="error" onClick={removeBoard}>
          Remover
        </Button>
      </Stack>
      <Divider />
      <Box padding={2} paddingBottom={0}>
      <Button variant="contained" fullWidth color="secondary" onClick={print}>
        Imprimir
      </Button>
      </Box>
    </Box>
  )
} 
