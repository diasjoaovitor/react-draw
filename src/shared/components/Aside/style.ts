import { SxProps, Theme } from "@mui/material"

export const aside: SxProps<Theme> = {
  width: '400px',
  minWidth: '340px',
  borderTop: '1px solid gray',
  overflowY: 'auto',
  paddingBottom: 2,
  '& .MuiInputBase-root': {
    marginBottom: 2
  }
}
