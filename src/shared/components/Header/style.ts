import { SxProps, Theme } from "@mui/material"

export const header: SxProps<Theme> = {
  justifyContent: "space-between",
  alignItems: "center",
  flexDirection: "row",
  padding: 2
}