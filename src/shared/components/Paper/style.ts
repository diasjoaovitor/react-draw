import { SxProps, Theme } from "@mui/material"
import { CSSProperties } from "react"

export const paper: SxProps<Theme> = {
  flex: 1,
  padding: 2,
  '& .paper': {
    backgroundColor: '#fff',
    margin: 'auto'
  },
  '& g': {
    cursor: 'pointer'
  }
}

export const svg: CSSProperties = { 
  fontFamily: 'Arial Black', 
  fontWeight: '900'
 }
 