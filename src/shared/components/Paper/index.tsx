import { createElement, MouseEvent } from 'react'
import { useResizeDetector } from 'react-resize-detector'
import { Box, Paper as MUIPaper } from '@mui/material'
import { TSVGGroup, TText } from '../../types'
import { getWidth } from '../../functions'
import { paper } from '../../states'
import * as S from './style'

type Props = {
  boards: TSVGGroup[]
  handleClick(e: MouseEvent<SVGAElement>): void
}

export const Paper: React.FC<Props> = ({ boards, handleClick }) => {
  const { height, ref } = useResizeDetector()

  if(!height) return <div ref={ref}>carregando...</div>

  const style = {
    height: `${height}px`,
    width: `${getWidth(height)}px`
  }

  return (
    <Box ref={ref} sx={S.paper}>
      <MUIPaper className="paper" sx={{ ...style }}>
        <svg viewBox={`0 0 ${paper.width} ${paper.height}`} style={S.svg}>
          {boards.map((board, index) => (
            <g 
              key={index} 
              id={`${index}`}
              transform={`
                scale(${board.transform.scale.join(' ')})
              `}
              onClick={handleClick}
            >
              {board.elements.map((element, index) => (
                createElement(element.type, { 
                  ...element.attributes, key: index
                }, (element as TText).textContent || '')
              ))}
            </g>
          ))}
        </svg>
      </MUIPaper>
    </Box>
  )
} 