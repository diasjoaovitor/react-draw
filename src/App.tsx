import { Box } from '@mui/material'
import { Aside, Header, Paper } from './shared/components'
import { ThemeProvider } from './shared/contexts'
import { useApp } from './shared/hooks'
import * as S from './shared/styles'

const App: React.FC = () => {
  const { 
    rect, board, boards,
    handleSelectChange, handleChange, handleTransform, handleBlur,
    newBoard, handleClick, removeBoard, print
   } = useApp()

  return (
    <ThemeProvider>
      <Header />
      <Box component="main" sx={S.app}>
        <Paper 
          boards={boards} 
          handleClick={handleClick}
        />
        <Aside 
          rect={rect}
          board={board} 
          handleSelectChange={handleSelectChange} 
          handleChange={handleChange} 
          handleBlur={handleBlur}
          handleTransform={handleTransform}
          newBoard={newBoard}
          removeBoard={removeBoard}
          print={print}
        />
      </Box>
    </ThemeProvider>
  )
}

export default App
